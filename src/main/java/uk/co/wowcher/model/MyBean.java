package uk.co.wowcher.model;

/**
 * Created by tony_murphy on 03/04/15.
 */
public class MyBean {

    private Long id;
    private String name;

    public MyBean(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}