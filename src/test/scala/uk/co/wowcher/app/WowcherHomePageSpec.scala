package uk.co.wowcher.app

import org.openqa.selenium.WebDriver
import org.scalatest.{Matchers, FlatSpec}
import org.scalatest.selenium.HtmlUnit

/**
 * Created by tony_murphy on 03/04/15.
 */
class WowcherHomePageSpec extends FlatSpec with Matchers with HtmlUnit {

  val host = "http://www.wowcher.co.uk"
  webDriver.setJavascriptEnabled(false)

  "The blog app home page" should "have the correct title" in {
    go to (host + "/deals/london")
    pageTitle should be ("Wowcher | Daily Deals - Save up to 80% on London deals")
  }

}

