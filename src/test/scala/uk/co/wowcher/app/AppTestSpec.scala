package uk.co.wowcher.app

import org.scalatest.{Tag, Matchers, FlatSpec}

object SlowTest extends Tag("com.mycompany.tags.SlowTest")


/**
 * Created by tony_murphy on 02/04/15.
 */
class AppTestSpec extends FlatSpec with Matchers {

  behavior of "App"

  it should "work" in {
    alert("hello")
    info("info")
    note("note")
  }

  it should "ignore" ignore {
    alert("hello")
    info("info")
    note("note")
  }

  it should "tag" taggedAs(SlowTest) in {
    alert("hello")
    info("info")
    note("note")
  }
}
