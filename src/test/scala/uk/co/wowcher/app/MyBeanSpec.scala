package uk.co.wowcher.app

import org.scalatest.{FlatSpec, Matchers}
import uk.co.wowcher.model.MyBean

/**
 * Created by tony_murphy on 03/04/15.
 */
class MyBeanSpec extends FlatSpec with Matchers {


  behavior of "MyBean"

  it must "construct correctly" in {

    val id: Long = 1L;
    val name: String = "hello"
    val myBean = new MyBean(id,name);
    info("working through the test")
    myBean.getId should equal(id)
    myBean.getName should equal(name)

  }

}
